import Fruit from "./Fruit";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Game extends cc.Component {
    @property({ type: cc.Prefab })
    fruit: cc.Prefab;
    @property({ type: cc.Node })
    line: cc.Node;

    private isTouch: boolean = false;
    private touchMoveLocal: cc.Vec2 = null;
    private touchStartLocal: cc.Vec2 = null;

    onLoad() {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        manager.enabledDebugDraw = true;
        manager.enabledDrawBoundingBox = true;
        cc.director.getPhysicsManager().enabled = true;
    }

    start() {
        this.initEvent();
    }
    initEvent(): void {
        this.node.on(cc.Node.EventType.TOUCH_START, this.uiNodeStartEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.uiNodeMoveEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.uiNodeEndEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.uiNodeEndEvent, this);
    }

    uiNodeStartEvent(e: cc.Event.EventTouch): void {
        this.touchStartLocal = null;
        this.touchMoveLocal = null;
        this.touchStartLocal = this.node.convertToNodeSpaceAR(e.getLocation());
        this.line.setPosition(this.touchStartLocal);
        this.line.width = 0;

    }
    //
    uiNodeMoveEvent(e: cc.Event.EventTouch): void {
        if (!this.touchStartLocal) return;
        console.log("移动");
        this.touchMoveLocal = this.node.convertToNodeSpaceAR(e.getLocation());
        //计算切割线旋转角度
        this.line.angle = -this.rotationTarget(this.touchStartLocal, this.touchMoveLocal) + 90;
        this.line.width = Math.abs(this.line.getPosition().sub(this.touchMoveLocal).mag());
    }

    //
    uiNodeEndEvent(e: cc.Event.EventTouch): void {
        if (!this.touchStartLocal) return;
        this.isTouch = false;
        //抬手以后进行射线检测
        let touchEndLocal: cc.Vec2 = this.node.convertToNodeSpaceAR(e.getLocation());
        let touchStartLocal: cc.Vec2 = this.node.convertToNodeSpaceAR(e.getStartLocation());
        this.recalcResults(0, e.getStartLocation(), e.getLocation());
        this.line.width = 0;
    }
    /**角度计算 */
    rotationTarget(startPos: cc.Vec2, endPos: cc.Vec2) {
        let direction = endPos.sub(startPos).normalize();
        let radian = direction.signAngle(cc.v2(1, 0));
        //将弧度转换为欧拉角
        let angle = radian / Math.PI * 180 + 90;
        //返回角度
        return angle;
    }
    //
    /**
     * 假设切割刚体的碰撞定点分别是rayStart和rayEnd,
     * 那么首先我们要通过rayCast()获取线段与刚体的碰撞点cutInPoint和cutOutPoint。
     * 但是rayCast()回调函数只包含一个碰撞点，
     * 因此需要交换线段的起点和终点，
     * 再次调动rayCast()来获取另外一个碰撞点。
     */
    recalcResults(type: number, touchStartLocal: cc.Vec2, touchEndLocal: cc.Vec2): void {
        let colliderArr = [];
        let result = cc.director.getPhysicsManager().rayCast(touchStartLocal, touchEndLocal, cc.RayCastType.All);
        for (let i = 0; i < result.length; i++) {
            let collider: any = result[i].collider;
            if (collider.tag > 0 && collider.tag < 100) {   //切割的是水果  并且切割的collider 没有切割
                let isExist: boolean = false;
                for (let j = 0; j < colliderArr.length; j++) {
                    if (collider == colliderArr[j]) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    colliderArr.push(collider);
                    let cutState = this.checkCutPolygon(type, touchStartLocal, touchEndLocal, collider);
                }
            }

        }
    }
    /**
     * @param touchStartLocal 
     * @param touchEndLocal 
     * @param collider 
     */
    private checkCutPolygon(type, touchStartLocal, touchEndLocal, collider): boolean {
        let body = collider.body;
        let points = collider.points;
        let tag: number = collider.tag;
        // 转化为本地坐标
        let localPoint1 = cc.Vec2.ZERO;
        let localPoint2 = cc.Vec2.ZERO;
        body.getLocalPoint(touchStartLocal, localPoint1);
        body.getLocalPoint(touchEndLocal, localPoint2);

        let isCut: boolean = false;
        let splitResults = [];
        let intersectPoint = [];
        this.lineCutPolygon(localPoint1, localPoint2, points, splitResults, intersectPoint);
        if (splitResults.length <= 0) {
            return false;
        }

        //回收本体
        collider.node.destroy();
        for (let j = 0; j < splitResults.length; j++) {
            let splitResult = splitResults[j];
            if (splitResult.length < 3) continue;
            this.cloneNode(collider.node, splitResult);
        }
       
    }
    /**
     * 线段对多边形进行切割
     * 返回多边形数组
     * 如果没有被切割  返回空数组
     */
    private lineCutPolygon(p1: cc.Vec2, p2: cc.Vec2, polygonPoints: cc.Vec2[], splitResults, intersectPoint: cc.Vec2[]): any {
        let points: cc.Vec2[] = [];
        let pointIndex: number[] = [];   //相交点的索引
        let intersectPoints: cc.Vec2[] = [];
        //将多边形所有的点以及交点组成一个点的序列
        for (let i = 0; i < polygonPoints.length; i++) {
            points.push(polygonPoints[i]);

            let a: cc.Vec2 = polygonPoints[i];
            let b: cc.Vec2 = polygonPoints[0];
            if (i < polygonPoints.length - 1) b = polygonPoints[i + 1];
            //计算当前点和下一个点组成的线段和p1,p2组成的线段是不是有交点，有的话是多少
            let c = this.lineCrossPoint(p1, p2, a, b);
            if (c[0] == 0) {   //相交
                pointIndex.push(points.length);
                points.push(c[1] as cc.Vec2);
                intersectPoints.push(c[1] as cc.Vec2);
            } else if (c[0] > 0) {
                if ((c[1] as cc.Vec2).equals(a)) {      //共有点
                    pointIndex.push(points.length - 1);
                    intersectPoints.push(points[points.length - 1]);
                }
                else {    //这种情况基本不存在
                    pointIndex.push(points.length);
                }
            }
        }
        //如果交点个数大于1 说明进行了切割
        if (pointIndex.length > 1) {
            //对切割垫进行排序
            //根据碰撞点的到p1 的距离,将切割点由近到远排列
            //冒泡排序操作
            for (let i = 0; i < intersectPoints.length; i++) {	//外层循环控制排序轮数
                for (let j = 0; j < intersectPoints.length - i - 1; j++) {
                    let dis1: number = this.getDisPoints(p1, intersectPoints[j]);
                    let dis2: number = this.getDisPoints(p1, intersectPoints[j + 1]);
                    if (dis1 > dis2) {
                        let temp = intersectPoints[j];
                        intersectPoints[j] = intersectPoints[j + 1];
                        intersectPoints[j + 1] = temp;
                    }
                }
            }
            intersectPoint.push(intersectPoints[0])
            intersectPoint.push(intersectPoints[intersectPoint.length - 1])
            //准备从第一个开始拆分 先弄清楚第一个交点是由内穿外，还是外穿内
            let cp0 = points[pointIndex[0]];
            let cp1 = points[pointIndex[1]];
            let r = this.relationPointToPolygon(new cc.Vec2((cp0.x + cp1.x) / 2, (cp0.y + cp1.y) / 2), polygonPoints);

            let inPolygon: boolean = r >= 0;
            if (pointIndex.length > 2 && this.getDisPoints(cp0, cp1) > this.getDisPoints(cp0, points[pointIndex[pointIndex.length - 1]])) {
                cp1 = points[pointIndex[pointIndex.length - 1]];
                r = this.relationPointToPolygon(new cc.Vec2((cp0.x + cp1.x) / 2, (cp0.y + cp1.y) / 2), polygonPoints);
                inPolygon = r < 0;
            }
            let firstInPolygon = inPolygon;//起始点是从外面穿到里面

            let index = 0;
            let startIndex = pointIndex[index];
            let oldPoints = [];
            let newPoints = [];
            let count = 0;

            oldPoints.push(points[startIndex]);
            if (inPolygon) {
                newPoints.push(points[startIndex]);
            }

            index++;
            count++;
            startIndex++;

            while (count < points.length) {
                if (startIndex == points.length) startIndex = 0;
                let p = points[startIndex];
                if (index >= 0 && startIndex == pointIndex[index]) {
                    //又是一个交点
                    index++;
                    if (index >= pointIndex.length) index = 0;
                    if (inPolygon) {
                        //原来是在多边形内部
                        //产生了新的多边形
                        newPoints.push(p);
                        splitResults.push(newPoints);
                        newPoints = [];
                    }
                    else {
                        //开始新的多边形
                        newPoints = [];
                        newPoints.push(p);
                    }
                    oldPoints.push(p);
                    inPolygon = !inPolygon;
                }
                else {
                    //普通的点
                    if (inPolygon) {
                        newPoints.push(p);
                    }
                    else {
                        oldPoints.push(p);
                    }
                }
                startIndex++;
                count++;
            }
            if (inPolygon) {
                if (!firstInPolygon && newPoints.length > 1) {
                    //如果起始点是从里面穿出去，到这里跟起始点形成闭包
                    newPoints.push(points[pointIndex[0]]);
                    splitResults.push(newPoints);
                }
                else {
                    //结束了，但是现在的状态是穿在多边形内部
                    //把newPoints里面的回复到主多边形中
                    for (let i = 0; i < newPoints.length; ++i) {
                        oldPoints.push(newPoints[i]);
                    }
                }

            }

            splitResults.push(oldPoints);
        }
        return splitResults;
    }

    //点和多边形的关系
    //返回值: -1:在多边形外部, 0:在多边形内部, 1:在多边形边线内, 2:跟多边形某个顶点重合
    private relationPointToPolygon(point: cc.Vec2, polygon: cc.Vec2[]) {
        let count = 0;
        for (let i = 0; i < polygon.length; ++i) {
            if (polygon[i].equals(point)) {
                return 2;
            }

            let pa = polygon[i];
            let pb = polygon[0];
            if (i < polygon.length - 1) {
                pb = polygon[i + 1];
            }

            let re = this.rayPointToLine(point, pa, pb);
            if (re == 1) {
                return 1;
            }
            if (re == 0) {
                count++;
            }
        }
        if (count % 2 == 0) {
            return -1;
        }
        return 0;
    }
    //点发出的右射线和线段的关系
    // 返回值: -1:不相交, 0:相交, 1:点在线段上
    private rayPointToLine(point: cc.Vec2, linePA: cc.Vec2, linePB: cc.Vec2) {
        // 定义最小和最大的X Y轴值  
        let minX = Math.min(linePA.x, linePB.x);
        let maxX = Math.max(linePA.x, linePB.x);
        let minY = Math.min(linePA.y, linePB.y);
        let maxY = Math.max(linePA.y, linePB.y);

        // 射线与边无交点的其他情况  
        if (point.y < minY || point.y > maxY || point.x > maxX) {
            return -1;
        }

        // 剩下的情况, 计算射线与边所在的直线的交点的横坐标  
        let x0 = linePA.x + ((linePB.x - linePA.x) / (linePB.y - linePA.y)) * (point.y - linePA.y);
        if (x0 > point.x) {
            return 0;
        }
        if (x0 == point.x) {
            return 1;
        }
        return -1;
    }

    // 
    cloneNode(node: cc.Node, splitResult: Array<any>): boolean {
        //更具节点名字在对象池中获取节点
        let isOk: boolean = false;
        let fruitNode: cc.Node = cc.instantiate(this.fruit);
        fruitNode.position = node.position;
        fruitNode.angle = node.angle;
        fruitNode.name = node.name;
        try {
            this.node.addChild(fruitNode);
            let collider = fruitNode.getComponent(cc.PhysicsPolygonCollider);
            fruitNode.getComponent(cc.PhysicsPolygonCollider).friction = 0.01;
            collider.points = splitResult;
            collider.apply();
            fruitNode.getComponent(Fruit).draw();
            // this.fruitIndex++;
            isOk = true;
        } catch (error) {
            console.log("出现异常--，克隆", error);
            fruitNode.destroy();
            isOk = false;
        }
        return isOk;
    }
    /**
     * 获取两点之间的距离
     */
    private getDisPoints(pos_start: cc.Vec2, pos_end: cc.Vec2): number {
        let dis: number = 0;
        //触摸点与起始点x,y轴的距离
        var x_distance = pos_start.x - pos_end.x;
        var y_distance = pos_start.y - pos_end.y;
        // 勾股定理求斜边
        dis = Math.sqrt(Math.pow(x_distance, 2) + Math.pow(y_distance, 2));
        return dis;
    }
    /**
     * 求两条线段的交点
     * @param p1 
     * @param p2 
     * @param q1 
     * @param q2 
     * 返回值：[n,p] n:0相交，1在共有点，-1不相交  p:交点
     */
    public lineCrossPoint(p1: cc.Vec2, p2: cc.Vec2, q1: cc.Vec2, q2: cc.Vec2) {
        let a = p1, b = p2, c = q1, d = q2;
        let s1, s2, s3, s4;
        let d1, d2, d3, d4;
        let p: cc.Vec2 = new cc.Vec2(0, 0);
        d1 = this.dblcmp(s1 = this.ab_cross_ac(a, b, c), 0);
        d2 = this.dblcmp(s2 = this.ab_cross_ac(a, b, d), 0);
        d3 = this.dblcmp(s3 = this.ab_cross_ac(c, d, a), 0);
        d4 = this.dblcmp(s4 = this.ab_cross_ac(c, d, b), 0);

        //如果规范相交则求交点
        if ((d1 ^ d2) == -2 && (d3 ^ d4) == -2) {
            p.x = (c.x * s2 - d.x * s1) / (s2 - s1);
            p.y = (c.y * s2 - d.y * s1) / (s2 - s1);
            return [0, p];
        }

        //如果不规范相交
        if (d1 == 0 && this.point_on_line(c, a, b) <= 0) {
            p = c;
            return [1, p];
        }
        if (d2 == 0 && this.point_on_line(d, a, b) <= 0) {
            p = d;
            return [1, p];
        }
        if (d3 == 0 && this.point_on_line(a, c, d) <= 0) {
            p = a;
            return [1, p];
        }
        if (d4 == 0 && this.point_on_line(b, c, d) <= 0) {
            p = b;
            return [1, p];
        }
        //如果不相交
        return [-1, null];
    }
    //求a点是不是在线段上，>0不在，=0与端点重合，<0在。
    public point_on_line(a, p1, p2) {
        return this.dblcmp(this.dot(p1.x - a.x, p1.y - a.y, p2.x - a.x, p2.y - a.y), 0);
    }

    private ab_cross_ac(a, b, c) //ab与ac的叉积
    {
        return this.cross(b.x - a.x, b.y - a.y, c.x - a.x, c.y - a.y);
    }
    private dot(x1, y1, x2, y2) {
        return x1 * x2 + y1 * y2;
    }
    private cross(x1, y1, x2, y2) {
        return x1 * y2 - x2 * y1;
    }
    private dblcmp(a: number, b: number) {
        if (Math.abs(a - b) <= 0.000001) return 0;
        if (a > b) return 1;
        else return -1;
    }
    update(dt) { }
}
