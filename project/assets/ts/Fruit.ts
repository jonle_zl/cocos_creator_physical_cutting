// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Fruit extends cc.Component {
    onLoad() {
    }
    start() {
        this.draw();
    }
    public draw() {
        //获取碰撞包围盒子的点
        let points: any = this.getComponent(cc.PhysicsPolygonCollider).points;
        let mask = this.getComponent(cc.Mask);
        let graphics: cc.Graphics = (<any>mask)._graphics;
        // // @ts-ignore
        // const grapics = mask._graphics;
        //获取绘制 grapics
        // let grapics:cc.Graphics=this.getComponent(cc.Graphics); 
        // 擦除之前绘制的所有内容的方法。
        graphics.clear();
        // console.log(points);
        // console.log(graphics);
        let len: number = points.length;
        //移动路径起点到坐标(x, y)
        graphics.moveTo(points[len - 1].x, points[len - 1].y);
        for (let i = 0; i < points.length; i++) {
            graphics.lineTo(points[i].x, points[i].y);
        }
        graphics.strokeColor.fromHEX('#000000');
        graphics.lineWidth = 2;
        graphics.fill();
        graphics.stroke();
    }
    update(dt) { }
}
